# Rushia Bot

This is a discord bot created for the purposes of making cool functions.

## Description

Rushia bot currently has two primary uses. The first being it's primary purpose of accessing up to date information about various hololive talents. This information includes new twitter poses, youtube videos, as well as queueing up different songs and covers into the voice chat. Currently, it is programmed for talents Hoshimachi Suisei, Usada Pekora, and Uruha Rushia. It also has an additional functionality to search the popular anime image boards safebooru and danbooru with different tags.

### Dependencies

- "@discordjs/opus": "^0.3.3",
- "cheerio": "^1.0.0-rc.10",
- "discord.js": "^12.5.3",
- "eslint": "^7.13.0",
- "ffmpeg-static": "^4.4.0",
- "node-fetch": "^2.6.4",
- "twitter": "^1.7.1",
- "ytdl-core-discord": "^1.3.1"

### Executing program

Create a new node project to generate the proper package files and copy the files into the appropriate folders. Simply run node * within the directory this bot is saved in

