const ytdl = require('ytdl-core-discord');
const Discord = require('discord.js');

async function playLink(message, path, queue) {
	// Join the same voice channel of the author of the message
	const voiceChannel = message.member.voice.channel;
	if (!voiceChannel) return 'A member must be present in a voice channel to play a youtube video';

	const permissions = voiceChannel.permissionsFor(message.client.user);
	if (!permissions.has('CONNECT')) return 'I don\'t have permission to connect to this channel';
	if (!permissions.has('SPEAK')) return 'I don\'t have permission to speak in this channel';

	const songInfo = await ytdl.getInfo(path);

	const song = {
		title: songInfo.videoDetails.title,
		url: songInfo.videoDetails.video_url,
		thumbnail: songInfo.videoDetails.thumbnails[4].url,
	}
	
	return await this.queueSong(message, song, queue);

}

async function queueSong(message, song, queue) {
	const serverQueue = queue.get(message.guild.id);
	const voiceChannel = message.member.voice.channel;

	// if there is no song queue on the server, make one
	if(!serverQueue) {
		const queueConstruct = {
			textChannel: message.channel,
			voiceChannel: voiceChannel,
			connection: null,
			songs: [song],
			volume: 50,
			playing: true,
		};
		
		// Test if the bot can join the correct voice channel
		try {
			const connection = await voiceChannel.join();
			queueConstruct.connection = connection;
			queue.set(message.guild.id, queueConstruct);
			return playSong(message, queueConstruct.songs[0], queue);
			
		} catch (error) {
			console.log(error);
			message.channel.send("There was an error joining the voice channel");
			queue.delete(message.guild.id);
			return;
		}

	} else {
		serverQueue.songs.push(song);
		const embed = new Discord.MessageEmbed()
			.setImage(song.thumbnail)
			.setAuthor("Added to Queue: ")
			.setTitle(song.title);
		try{
			message.channel.send(embed);
		}
		catch (error) {
			console.log(error);
			message.channel.send('There was an error processing the embed for the song in queue');
		}
		return;
	}
	return;
}

async function playSong(message, song, queue) {
	const serverQueue = queue.get(message.guild.id);

	if(!song) {
		serverQueue.textChannel.send("There are no songs in the queue!");
		serverQueue.voiceChannel.leave();
		queue.delete(message.guild.id);
		return;
	}

	const embed = new Discord.MessageEmbed()
		.setImage(serverQueue.songs[0].thumbnail)
		.setAuthor("Now Playing: ")
		.setTitle(serverQueue.songs[0].title);
	try{
		message.channel.send(embed);
	}
	catch (error) {
		console.log(error);
		message.channel.send('There was an error processing the embed for the song that is currently playing');
	}
	
	serverQueue.connection
		.play(
			await ytdl(song.url), {type: 'opus'})
		.on("finish", () => {
			serverQueue.songs.shift();
			playSong(message, serverQueue.songs[0], queue);
			return;
		})
		.on("error", console.error)
		.setVolumeLogarithmic(serverQueue.volume/250);
}

module.exports = {
	playLink,
	queueSong,
	playSong
}