const Discord = require('discord.js');
const fetch = require('node-fetch');
const { youtubeapi, twitter } = require('../../api.json');
const Twitter = require('twitter');

async function sfwSearch(tags) {
	// Request using created URL

	const requestURL = 'https://safebooru.donmai.us/posts.json?random=true&limit=1&tags=' + tags + '';
	const imageData = await fetch(requestURL).then(response => response.json());

	// Pick from the images given
	const imageNumber = Math.floor(Math.random() * imageData.length);
	const post = imageData[imageNumber];

	// Use Image Data to create URL
	const image_url = post.file_url;
	const post_url = 'https://safebooru.donmai.us/posts/' + post.id;
	const footer = 'Size: ' + post.image_width + ' x ' + post.image_height;

	const embed = new Discord.MessageEmbed()
		.setImage(image_url)
		.setFooter(footer);

	const requestInfo = { 'embed' : embed, 'post_url' : post_url};

	return requestInfo;
}

async function nsfwSearch(tags) {
	// Request using created URL
	const requestURL = 'https://danbooru.donmai.us/posts.json?random=true&tags=' + tags + '+rating%3Aexplicit';

	const imageData = await fetch(requestURL).then(response => response.json());

	console.log(imageData);

	// Pick from the images given
	const imageNumber = Math.floor(Math.random() * imageData.length);
	const post = imageData[imageNumber];

	// Use Image Data to create URL
	const image_url = post.file_url;
	const post_url = 'https://danbooru.donmai.us/posts/' + post.id;
	const footer = 'Size: ' + post.image_width + ' x ' + post.image_height;

	const embed = new Discord.MessageEmbed()
		.setImage(image_url)
		.setFooter(footer);

	const requestInfo = { 'embed' : embed, 'post_url' : post_url };

	return requestInfo;
}

async function recentVideo(channel_id) {
	const requestURL = 'https://www.googleapis.com/youtube/v3/search?key=' + youtubeapi + '&channelId=' + channel_id + '&part=snippet,id&order=date&maxResults=10';

	const searchData = await fetch(requestURL)
		.then(response => response.json());
	console.log(searchData);

	const videos = searchData['items'];
	let iterator = 0;
	let recentVideo = videos[iterator];

	while (recentVideo.snippet.liveBroadcastContent === 'upcoming' && iterator < 9) {
		iterator++;
		recentVideo = videos[iterator];
	}

	const videoLink = 'https://www.youtube.com/watch?v=' + recentVideo.id.videoId;
	const videoDate = recentVideo.snippet.publishTime.split('T');

	const embed = new Discord.MessageEmbed()
		.setTitle(recentVideo.snippet.title)
		.setURL(videoLink)
		.setImage(recentVideo.snippet.thumbnails.high.url)
		.setFooter('Date Posted: ' + videoDate[0] + ' ' + videoDate[1].slice(0, -1));

	return embed;
}

async function recentTweet(name) {

	const client = new Twitter({
		consumer_key: twitter.api,
		consumer_secret: twitter.secretapi,
		bearer_token: twitter.bearertoken,
	});

	// console.log(client);
	const params = {
		screen_name: name,
		count: 10,
		exclude_replies: true,
		include_rts: false,
	};

	return new Promise((resolve, reject) => {
		client.get('statuses/user_timeline', params, function(error, tweets) {
			if (error) {reject(error);}

			const singleTweet = tweets[0];
			const tweetText = singleTweet.text;

			// Date tweet was created
			const date = singleTweet.created_at;

			if (singleTweet.extended_entities) {
				// If the media is a mp4
				if (singleTweet.extended_entities.media[0].video_info) {
					const video = singleTweet.extended_entities.media[0].video_info.variants[3].url;
					const image = singleTweet.extended_entities.media[0].media_url;
					resolve({ text: tweetText, date: date, image: image, video: video });
				}
				// If the media is an image
				else if (singleTweet.extended_entities.media[0].sizes) {
					const image = singleTweet.extended_entities.media[0].media_url;
					resolve({ text: tweetText, date: date, image: image });
				}

			}

			resolve({ text: tweetText, date: date });
		});
	});

	// Can include response in parameters and console.log(response) to see 
	// the raw response object
}

/*
async grabThumbnail(video_id) {
	const requestURL = 'https://www.googleapis.com/youtube/v3/videos?key=' + youtubeapi + '&id=' + video_id + '&part=snippet,id';

	const searchData = await fetch(requestURL).then(response => response.json());
	const video = searchData['items'][0];
	return video.snippet.thumbnails.high.url;
},*/

module.exports = {
	sfwSearch,
	nsfwSearch,
	recentVideo,
	recentTweet
};

