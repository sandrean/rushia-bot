const Discord = require('discord.js');
const helper = require('./helpers/hololive.js');
const audio = require('./helpers/audio.js');
const { suiseiSongs } = require('./helpers/songs.json');

module.exports = {
	name: 'hoshimachisuisei',
	aliases: ['suisei', 'hoshimachi', 'hoshimati'],
	cooldown: 1,
	description: 'The default command with no argument generates a random picture of Hoshimachi Suisei from Hololive',
	async execute(message, args, queue) {
		const condense = args.join('').toLowerCase();
		const profileImage = 'https://pbs.twimg.com/profile_images/1429100173351788549/nK13pWFz_400x400.jpg';
		let embed = new Discord.MessageEmbed()
			.setColor('#2a2a81');

		if (!args.length || condense === 'safe') {
			const searchInfo = await helper.sfwSearch('hoshimachi_suisei');
			embed = searchInfo.embed;

			embed.setTitle('ほしまちぎゃらりー');
			embed.setAuthor('Safebooru: Hoshimachi Suisei', profileImage, searchInfo.post_url);
			
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'nsfw') {
			const nsfwInfo = await helper.nsfwSearch('hoshimachi_suisei');
			embed = nsfwInfo.embed;

			embed.setTitle('ほしまちぎゃらりー');
			embed.setAuthor('Danbooru: Hoshimachi Suisei', profileImage, nsfwInfo.post_url);
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'recent' || condense === 'video') {

			embed = await helper.recentVideo('UC5CwaMl1eIgY8h02uZw7u8A');
			embed.setAuthor('Suisei\'s Most Recent Video', profileImage);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'tweet' || condense === 'twitter') {
			const tweetInfo = await helper.recentTweet('suisei_hosimati');

			if (tweetInfo.video) {
				const videoEmbed = new Discord.MessageEmbed()
					.setTitle('This tweet comes with a video')
					.setDescription('Discord currently does not support mp4s in embeds, but allows them to be sent in the text channel and played at a lower bit rate');
				message.channel.send(videoEmbed);
				message.channel.send(tweetInfo.video);
			}
			if (tweetInfo.image) {
				embed.setImage(tweetInfo.image);
			}

			embed.setAuthor('@suisei_hosimati\'s Most Recent Tweet', profileImage);
			embed.setDescription(tweetInfo.text);
			embed.setFooter('Sent on ' + tweetInfo.date);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense in suiseiSongs) {
			try {
				const songInfo = await isSong(condense);
				const songUrl = 'https://www.youtube.com/watch?v=' + songInfo.link;
				await audio.playLink(message, songUrl, queue);	
			} 
			catch (error) {
				console.log(error);
			}
		}
		else if(condense === 'args' | condense === 'arg' | condense === 'help') {
			embed.setAuthor('hoshimachisuisei command', profileImage);
			embed.addFields(
				{ name: 'no arg | safe', value: 'Returns a image of Hoshimachi Suisei from safebooru' },
				{ name: 'recent | video', value: 'Returns Suisei\'s most recently posted video' },
				{ name: '<songname>', value: 'The bot joins the voice channel you are currently in and plays one of Suisei\'s song\nNote: This command will not work if you are not in a voice channel' },
				{ name: 'twitter | tweet', value: 'Returns Suisei\'s most recent tweet @suisei_hosimati' },
				{ name: 'nsfw', value: 'Returns a NSFW image of Hoshimachi Suisei from danbooru' },
				{ name: 'args | help', value: 'Displays all arguments for the hoshimachisuisei command' },
			);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}

		}
		else {
			embed.setTitle('Invalid Argument!');
			embed.setAuthor('Hoshimachi Suisei Command', profileImage);
			embed.setDescription('Please do use the argument \'args\' to see the proper argument usage of this command');
			
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
	},
};

async function isSong(input) {
	if(input in suiseiSongs) {
		return suiseiSongs[input];
	}
	for (const key in suiseiSongs) {
		if(suiseiSongs[key].aliases.includes(input)) {
			return suiseiSongs[key];
		}
	}           
	return;    
}