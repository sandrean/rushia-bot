const Discord = require('discord.js');
const fetch = require('node-fetch');

module.exports = {
	name: 'safebooru',
	aliases: ['sb', 'sfb', 'safe'],
	cooldown: 1,
	description: 'Search for an image on the online anime image library, safebooru.donmai.us, in safe mode. This search is limited to a maximum of 2 tags',
	async execute(message, args) {
		if (args.length > 2) {
			await message.reply('there cannot be more than two arguments given');
			return;
		}
		const tags = args.join('+');
		// Request using created URL
		const requestURL = 'https://safebooru.donmai.us/posts.json?random=true&tags=' + tags;

		try {
			await fetch(requestURL).then(response => response.json());
		}
		catch (error) {
			await message.reply('there was an error proccessing your search arguments, please enter valid arguments');
		}

		const imageData = await fetch(requestURL).then(response => response.json());

		// Pick from the images given
		const imageNumber = Math.floor(Math.random() * imageData.length);
		const post = imageData[imageNumber];

		// Use Image Data to create URL
		const image_url = post.file_url;
		const post_url = 'https://safebooru.donmai.us/posts/' + post.id;
		const footer = 'Size: ' + post.image_width + ' x ' + post.image_height;

		const embed = new Discord.MessageEmbed()
			.setColor('#FFFFFF')
			.setAuthor('Safebooru', 'https://i.imgur.com/ytMyEyr.png', post_url)
			.setImage(image_url)
			.setFooter(footer);

		try{
			await message.channel.send(embed);
		}
		catch (error) {
			await message.channel.send('There was an error proccessing this command');
		}

	},
};

