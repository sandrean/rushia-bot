const Discord = require('discord.js');
const fetch = require('node-fetch');

module.exports = {
	name: 'safebooruorg',
	aliases: ['sborg', 'sfborg'],
	cooldown: 1,
	description: 'Search for an image on the online anime image library, safebooru.org. While this library is not as large as the one found on safebooru.donmai.us, their api allows the user to search for multiple tags at the same time',
	async execute(message, args) {
		const tags = args.join('+');
		// Request using created URL
		const requestURL = 'https://safebooru.org/index.php?page=dapi&s=post&q=index&json=1&tags=' + tags;

		try {
			await fetch(requestURL).then(response => response.json());
		}
		catch (error) {
			await message.channel.send('There was an error proccessing your search arguments, please enter valid arguments');
		}

		const imageData = await fetch(requestURL).then(response => response.json());

		// Pick from the images given
		const imageNumber = Math.floor(Math.random() * imageData.length);
		const post = imageData[imageNumber];

		// Use Image Data to create URL
		const image_url = 'https://safebooru.org//images/' + post.directory + '/' + post.image + '?' + post.id;
		const post_url = 'http://safebooru.org/index.php?page=post&s=view&id=' + post.id;
		const footer = 'Size: ' + post.width + ' x ' + post.height;

		const embed = new Discord.MessageEmbed()
			.setColor('#FFFFFF')
			.setAuthor('Safebooru: ', post_url)
			.setImage(image_url)
			.setFooter(footer);

		try{
			await message.channel.send(embed);
		}
		catch (error) {
			await message.channel.send('There was an error proccessing this command');
		}

	},
};

