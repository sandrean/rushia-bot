const { prefix } = require('../config.json');
const Discord = require('discord.js');

module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command.',
	aliases: ['commands'],
	usage: '[command name]',
	cooldown: 5,
	args: false,
	async execute(message, args) {
		const data = [];
		const { commands } = message.client;

		if (!args.length) {
			data.push('Here\'s a list of all my commands:');
			data.push(commands.map(command => command.name).join(', '));
			data.push(`\nYou can send \`${prefix}help [command name]\` to get info on a specific command!`);

			return message.author.send(data, { split: true })
				.then(() => {
					if (message.channel.type === 'dm') return;
					message.reply('I\'ve sent you a DM with all my commands!');
				})
				.catch(error => {
					console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
					message.reply('It seems like I can\'t DM you! Do you have DMs disabled?');
				});
		}
		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

		if (!command) {
			return message.reply('that\'s not a valid command!');
		}

		// Create new help embed
		const helpEmbed = new Discord.MessageEmbed()
			.setColor('#0099ff')
			.setTitle(`${command.name}`)
			.setDescription('This command has no other aliases');

		if (command.description) {
			helpEmbed.setDescription(`${command.description}`);
		}
		if (command.aliases) {
			helpEmbed.addField('Aliases', `${command.aliases.join(', ')}`, false);
		}
		if (command.usage) {
			helpEmbed.addField('Usage', '`' + `${prefix}${command.name} ${command.usage}` + '`', false);
		}
		else {
			helpEmbed.addField('Usage', '`' + `${prefix}${command.name}` + '`', false);
		}
		helpEmbed.addField('Cooldown: ', `${command.cooldown || 3}` + ' seconds', false);

		await message.channel.send(embed = helpEmbed);
	},
};