const Discord = require('discord.js');
const helper = require('./helpers/hololive.js');
const audio = require('./helpers/audio.js');
const { rushiaSongs } = require('./helpers/songs.json');

module.exports = {
	name: 'uruharushia',
	aliases: ['rushia', 'uruha'],
	cooldown: 1,
	description: 'The default command with no argument generates a random picture of Uruha Rushia from Hololive\'s Third Generation',
	async execute(message, args, queue) {
		const condense = args.join('').toLowerCase();
		const profileImage = 'https://pbs.twimg.com/profile_images/1302970943145275393/-agRJgRe_400x400.jpg';
		let embed = new Discord.MessageEmbed()
			.setColor('#6dccb5');
		if (!args.length || condense === 'safe') {
			const searchInfo = await helper.sfwSearch('uruha_rushia');
			embed = searchInfo.embed;

			embed.setTitle('絵クロマンサー');
			embed.setAuthor('Safebooru: Uruha Rushia', profileImage, searchInfo.post_url);
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'nsfw') {
			const nsfwInfo = await helper.nsfwSearch('uruha_rushia');
			embed = nsfwInfo.embed;

			embed.setTitle('絵クロマンサー');
			embed.setAuthor('Danbooru: Uruha Rushia', profileImage, nsfwInfo.post_url);
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'recent' || condense === 'video') {

			embed = await helper.recentVideo('UCl_gCybOJRIgOXw6Qb4qJzQ');
			embed.setAuthor('Rushia\'s Most Recent Video', profileImage);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
			
		}
		else if(condense === 'tweet' || condense === 'twitter') {
			const tweetInfo = await helper.recentTweet('uruharushia');

			if (tweetInfo.video) {
				const videoEmbed = new Discord.MessageEmbed()
					.setTitle('This tweet comes with a video')
					.setDescription('Discord currently does not support mp4s in embeds, but allows them to be sent in the text channel and played at a lower bit rate');
				message.channel.send(videoEmbed);
				message.channel.send(tweetInfo.video);
			}
			if (tweetInfo.image) {
				embed.setImage(tweetInfo.image);
			}

			embed.setAuthor('@uruharushia\'s Most Recent Tweet', profileImage);
			embed.setDescription(tweetInfo.text);
			embed.setFooter('Sent on ' + tweetInfo.date);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(isSong(condense) !== null) {
			try {
				const songInfo = await isSong(condense);
				const songUrl = 'https://www.youtube.com/watch?v=' + songInfo.link;
				await audio.playLink(message, songUrl, queue);	
			} 
			catch (error) {
				console.log(error);
			}
		}
		else if(condense === 'args' | condense === 'arg' | condense === 'help') {
			embed.setAuthor('uruharushia command', profileImage);
			embed.addFields(
				{ name: 'no arg | safe', value: 'Returns a image of Uruha Rushia from safebooru' },
				{ name: 'recent | video', value: 'Returns Rushia\'s most recently posted video' },
				{ name: 'ghost | ghostinaflower', value: 'The bot joins the voice channel you are currently in and plays Rushia\'s cover of Ghost in a Flower \nNote: This command will not work if you are not in a voice channel' },
				{ name: 'twitter | tweet', value: 'Returns Rushia\'s most recent tweet @uruharushia' },
				{ name: 'nsfw', value: 'Returns a NSFW image of Uruha Rushia from danbooru' },
				{ name: 'args | help', value: 'Displays all arguments for the uruharushia command' },
			);
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else {
			embed.setTitle('Invalid Argument!');
			embed.setAuthor('Uruha Rushia Command', profileImage);
			embed.setDescription('Please do use the argument \'args\' to see the proper argument usage of this command');
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
	},
};

async function isSong(input) {
	if(Object.prototype.hasOwnProperty.call(rushiaSongs, input)) {
		return rushiaSongs[input];
	}
	for (const key in rushiaSongs) {
		if(rushiaSongs[key].aliases.includes(input)) {
			return rushiaSongs[key];
		}
	}
	return null;
}
