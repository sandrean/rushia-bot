const Discord = require('discord.js');
const helper = require('./helpers/hololive.js');
const audio = require('./helpers/audio.js');
const { pekoraSongs } = require('./helpers/songs.json');

module.exports = {
	name: 'usadapekora',
	aliases: ['peko', 'pekora', 'usada'],
	cooldown: 1,
	description: 'The default command with no argument generates a random picture of Usada Pekora from Hololive\'s Third Generation',
	async execute(message, args, queue) {
		const condense = args.join('').toLowerCase();
		const profileImage = 'https://pbs.twimg.com/profile_images/1417395387527098371/sp6Dzzep_400x400.jpg';
		let embed = new Discord.MessageEmbed()
			.setColor('#b0bdea');
			
		if (!args.length || condense === 'safe') {
			const searchInfo = await helper.sfwSearch('usada_pekora');
			embed = searchInfo.embed;

			embed.setTitle('ぺこらーと');
			embed.setAuthor('Safebooru: Usada Pekora', profileImage, searchInfo.post_url);
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'nsfw') {
			const nsfwInfo = await helper.nsfwSearch('usada_pekora');
			embed = nsfwInfo.embed;

			embed.setTitle('ぺこらーと');
			embed.setAuthor('Danbooru: Usada Pekora', profileImage, nsfwInfo.post_url);
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'recent' || condense === 'video') {

			embed = await helper.recentVideo('UC1DCedRgGHBdm81E1llLhOQ');
			embed.setAuthor('Pekora\'s Most Recent Video', profileImage);
			
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense === 'tweet' || condense === 'twitter') {
			const tweetInfo = await helper.recentTweet('usadapekora');

			if (tweetInfo.video) {
				const videoEmbed = new Discord.MessageEmbed()
					.setTitle('This tweet comes with a video')
					.setDescription('Discord currently does not support mp4s in embeds, but allows them to be sent in the text channel and played at a lower bit rate');
				message.channel.send(videoEmbed);
				message.channel.send(tweetInfo.video);
			}
			if (tweetInfo.image) {
				embed.setImage(tweetInfo.image);
			}

			embed.setAuthor('@usadapekora\'s Most Recent Tweet', profileImage);
			embed.setDescription(tweetInfo.text);
			embed.setFooter('Sent on ' + tweetInfo.date);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else if(condense in pekoraSongs) {
			try {
				const songInfo = await isSong(condense);
				const songUrl = 'https://www.youtube.com/watch?v=' + songInfo.link;
				await audio.playLink(message, songUrl, queue);	
			} 
			catch (error) {
				console.log(error);
			}
		}
		else if(condense === 'args' | condense === 'arg' | condense === 'help') {
			embed.setAuthor('usadapekora command', profileImage);
			embed.addFields(
				{ name: 'no arg | safe', value: 'Returns a image of Usada Pekora from safebooru' },
				{ name: 'recent | video', value: 'Returns Pekora\'s most recently posted video' },
				{ name: '<songname>', value: 'The bot joins the voice channel you are currently in and plays one of Pekora\'s songs \nNote: This command will not work if you are not in a voice channel' },
				{ name: 'twitter | tweet', value: 'Returns Pekora\'s most recent tweet @usadapekora' },
				{ name: 'nsfw', value: 'Returns a NSFW image of Usada Pekora from danbooru' },
				{ name: 'args | help', value: 'Displays all arguments for the usadapekora command' },
			);

			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
		else {
			embed.setTitle('Invalid Argument!');
			embed.setAuthor('Usada Pekora Command', profileImage);
			embed.setDescription('Please do use the argument \'args\' to see the proper argument usage of this command');
		
			try{
				await message.channel.send(embed);
			}
			catch (error) {
				await message.channel.send('There was an error processing the embed for the ' + module.exports.name + ' command');
			}
		}
	},
};

async function isSong(input) {
	if(input in pekoraSongs) {
		return pekoraSongs[input];
	}
	for (const key in pekoraSongs) {
		if(input in pekoraSongs[key].aliases) {
			return pekoraSongs[key];
		}
	}
	return;    
}
